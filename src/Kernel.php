<?php

namespace FikiTeamcityGitlab;

use FikiTeamcityGitlab\Http\Response\Response;

class Kernel
{
    public function __construct()
    {
    }

    public function run()
    {
        $route = $_SERVER['REQUEST_URI'];

        $content = <<<EOL
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h1>This is php application</h1>
<p>You are currently on route: "$route"</p>
</body>
</html>
EOL;

        return new Response($content);
    }
}