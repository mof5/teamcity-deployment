<?php


namespace FikiTeamcityGitlab\Http\Response;


class Response
{

    /**
     * Response constructor.
     * @param string|null $content
     * @param int $status
     */
    public function __construct(
        private ?string $content = null,
        private int $status = 200,
    )
    {
    }

    /**
     * Display content
     */
    public function send()
    {
        echo $this->content;
    }
}